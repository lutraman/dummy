FROM mhart/alpine-node:11

RUN mkdir -p /opt/app

COPY app.js /opt/app

WORKDIR /opt/app

CMD [ "node", "app" ]

