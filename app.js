const http = require('http');
const fs = require('fs');

const port = process.env['PORT'] || 8080;
const message = process.env['MESSAGE'] || 'NOMESSAGE'
const log_file = process.env['LOG_FILE']

const log_handle = log_file ?
    fs.createWriteStream(log_file) :
    process.stdout

function log(msg) {
  log_handle.write(msg + '\n')
}

function tearDown() {
  if(log_handle !== process.stdout) {
    log_handle.end();
  }
  process.exit(0)
}
process.on('SIGTERM', tearDown)
process.on('SIGINT', tearDown)

http.createServer((req, res) => {
  res.write(message + '\n');
  res.end();
  log('served a client!')
}).listen(port);
